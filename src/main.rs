mod averages;
use std::io;

fn main() {
    // return mean, median, mode of a vector of integers
    let mut integers: Vec<i32> = vec![];

    println!("Mean, median, mode!");
    println!("Add an integer, or press enter (or input any non-integer character) to complete the calculations.");

    loop {
        println!("{:?}", integers);

        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        let number: i32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => break,
        };

        integers.push(number);
    }

    let (mean, median, mode) = averages::mean_median_mode(integers);
    println!("mean: {}", mean);
    println!("median: {}", median);
    println!("mode: {:?}", mode);
}
