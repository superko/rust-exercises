use std::collections::HashMap;

pub fn mean_median_mode(numbers: Vec<i32>) -> (f64, f64, Vec<i32>) {
    (mean(&numbers), median(&numbers), mode(&numbers))
}

pub fn mean(numbers: &Vec<i32>) -> f64 {
    let count = numbers.len();
    let sum: i32 = numbers.iter().sum();
    (sum as f64) / (count as f64)
}

pub fn median(numbers: &Vec<i32>) -> f64 {
    let mut sorted_numbers = numbers.clone();
    sorted_numbers.sort();

    let count = sorted_numbers.len();
    match count % 2 == 0 {
        // Note: integer division rounds down, so `count/2` will just chuck the decimal
        // As a result, `count/2` gives the median's index when number of entries is odd
        false => (sorted_numbers[count / 2]) as f64,
        // and when even, `count/2` is the upper of the two median values
        true => ((sorted_numbers[(count / 2) - 1] + sorted_numbers[count / 2]) / 2) as f64,
    }
}

pub fn mode(numbers: &Vec<i32>) -> Vec<i32> {
    let mut counts = HashMap::new();
    let mut modes: Vec<i32> = vec![];
    let mut highest_count = 0;

    for n in numbers {
        let count = counts.entry(n).or_insert(0);
        *count += 1;

        if *count > highest_count {
            highest_count = *count;
        }
    }

    for (n, count) in counts {
        if count == highest_count {
            modes.push(*n);
        }
    }
    modes
}
